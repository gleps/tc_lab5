%% init
clear all;
fs = 1e3;
dt = 1/fs;
t = 0:dt:1;

M = 1; % modulation depth

car_freq = 1e2;
car_omega = 2*pi*car_freq;
car_phi = 0;
car_sig = cos(car_omega*t + car_phi);

inf_freq = 10;
inf_omega = 2*pi*inf_freq;
inf_sig = cos(inf_omega*t);
phasedev = 3;
freqdev = 50;

%% inf_sig
plot_range = 1:500;
figure;
img = plot(t(plot_range), inf_sig(plot_range));
saveas(img, 'report/fig/inf_sig.png');

%% pmmod
mod_sig = pmmod(inf_sig, car_freq, fs, phasedev);

plot_range = 1:200;
figure;
hold on;
plot(t(plot_range), inf_sig(plot_range), 'linestyle', ...
	'--', 'color', 'r', 'LineWidth', 2);
img = plot(t(plot_range), mod_sig(plot_range));
saveas(img, 'report/fig/pmmod_sig.png');

%% modulated signal spectrum
Nfft = 2^nextpow2(length(mod_sig));
f = (0:Nfft-1)/Nfft*fs;

mod_sp = abs(fft(mod_sig, Nfft))/(Nfft/2);

figure;
lbound = 1;
hbound = 200;
img = plot(f(lbound:hbound), mod_sp(lbound:hbound));
xlabel('frequency');
ylabel('magnitude');
title('modulated signal spectrum');
saveas(img, 'report/fig/pmmod_sp.png');

%% pmdemod
demod_sig = pmdemod(mod_sig, car_freq, fs, phasedev);

img = plot(t(plot_range), demod_sig(plot_range));
saveas(img, 'report/fig/pmdemod_sig.png');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fmmod
mod_sig = fmmod(inf_sig, car_freq, fs, freqdev);

plot_range = 1:200;
figure;
hold on;
plot(t(plot_range), inf_sig(plot_range), 'linestyle', ...
	'--', 'color', 'r', 'LineWidth', 2);
img = plot(t(plot_range), mod_sig(plot_range));
saveas(img, 'report/fig/fmmod_sig.png');

%% modulated signal spectrum
Nfft = 2^nextpow2(length(mod_sig));
f = (0:Nfft-1)/Nfft*fs;

mod_sp = abs(fft(mod_sig, Nfft))/(Nfft/2);

figure;
lbound = 1;
hbound = 200;
img = plot(f(lbound:hbound), mod_sp(lbound:hbound));
xlabel('frequency');
ylabel('magnitude');
title('modulated signal spectrum');
saveas(img, 'report/fig/fmmod_sp.png');

%% fmdemod
demod_sig = fmdemod(mod_sig, car_freq, fs, freqdev);

plot_range = 1:200;
figure;
img = plot(t(plot_range), demod_sig(plot_range));
saveas(img, 'report/fig/fmdemod_sig.png');

